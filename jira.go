package jiraCacheClient

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	jira "github.com/andygrunwald/go-jira"
	"github.com/go-redis/redis"
)

// JiraCacheClient wraps a jira client to cache its session
type JiraCacheClient struct {
	redisClient *redis.Client
	transport   *jira.CookieAuthTransport
	saved       bool
}

// NewClient creates a new Jira client that caches its session
func NewClient(username, password, url, redisAddress string) (*jira.Client, error) {
	client := &JiraCacheClient{
		transport: &jira.CookieAuthTransport{
			Username: username,
			Password: password,
			AuthURL:  fmt.Sprintf("%s/rest/auth/1/session", url),
		},
	}

	if redisAddress != "" {
		client.redisClient = redis.NewClient(&redis.Options{Addr: redisAddress})

		session, err := client.redisClient.Get("jiraSession").Result()
		if err == nil {
			_ = json.Unmarshal([]byte(session), &client.transport.SessionObject)
		}
	}

	return jira.NewClient(&http.Client{Transport: client}, url)
}

// RoundTrip executes a single HTTP transaction, returning a Response for the provided Request.
func (jcc *JiraCacheClient) RoundTrip(req *http.Request) (*http.Response, error) {
	resp, err := jcc.transport.RoundTrip(req)
	if jcc.redisClient != nil && !jcc.saved {
		session, _ := json.Marshal(jcc.transport.SessionObject)
		_ = jcc.redisClient.Set("jiraSession", session, time.Hour).Err()
		jcc.saved = true
	}

	return resp, err
}
